//
//  GameViewController.swift
//  SceneDrawDemo
//
//  Created by Prateek Sharma on 6/26/19.
//  Copyright © 2019 Prateek Sharma. All rights reserved.
//

import UIKit
import QuartzCore
import SceneKit

class GameViewController: UIViewController {

    // The main view in which the scenekit's 3D view is loaded
    lazy private var gameView: SCNView = {
        let scnView = view as! SCNView
        // To allow moving the camera's point of view
        scnView.allowsCameraControl = true
        scnView.autoenablesDefaultLighting = true
        scnView.delegate = self
        return scnView
    }()
    // scenekit's 3D view which contains our game
    private var gameScene = SCNScene()
    // Camera node used to define the point of view in the game
    private var cameraNode: SCNNode {
        let node = SCNNode()
        node.camera = SCNCamera()
        node.position = SCNVector3(0, 5, 10)
        return node
    }
    // Used to add a time buffer for adding new elements
    private var targetCreationNode: TimeInterval = 0
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gameView.scene = gameScene
        gameView.isPlaying = true
        gameScene.rootNode.addChildNode(cameraNode)
    }
    
    private func addPyramidNode() {
        // Create a geometry / shape
        let shape = SCNPyramid(width: 1, height: 1, length: 1)
        let randomColor = UIColor(red: CGFloat(arc4random_uniform(126) + 125) / 255, green: CGFloat(arc4random_uniform(126) + 125) / 255, blue: CGFloat(arc4random_uniform(126) + 125) / 255, alpha: 1)
        // Set it's texture
        shape.firstMaterial?.diffuse.contents = randomColor
        // Create a scenekit's node
        let shapeNode = SCNNode(geometry: shape)
        
        // Create the physicsbody to know how it will react if an force/collision is acted on it
        // Sending Nil here will automatically let Scenekit decode the kind of shape it has
        shapeNode.physicsBody = SCNPhysicsBody(type: .dynamic, shape: nil)
        gameScene.rootNode.addChildNode(shapeNode)
        
        // Creating a force vector and applying it on the node as a impulse
        let force = SCNVector3(arc4random_uniform(2) == 1 ? 1 : -1, 14, arc4random_uniform(2) == 1 ? 0.2 : -0.2)
        shapeNode.physicsBody?.applyForce(force, at: SCNVector3(0.05, 0.05, 0.05), asImpulse: true)
    }
    
    private func removeStaleNodes() {
        // Removing node which are invisible but still present in the scene and memory
        for node in gameScene.rootNode.childNodes {
            guard node.presentation.position.y < -2  else { continue }
            node.removeFromParentNode()
        }
    }
}

extension GameViewController: SCNSceneRendererDelegate {
    
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        if time > targetCreationNode {
            gameView.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(100)) / 255, green: CGFloat(arc4random_uniform(100)) / 255, blue: CGFloat(arc4random_uniform(100)) / 255, alpha: 1)
            addPyramidNode()
            targetCreationNode = time + 0.6
        }
        removeStaleNodes()
    }
    
}

extension GameViewController {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first,
              let hitObject = gameView.hitTest(touch.location(in: gameView), options: nil).first
        else { return }
        hitObject.node.removeFromParentNode()
    }
    
}
